const config = require('./config')

const express = require('express')
	, stylus = require('stylus')
	, bodyParser = require('body-parser')
	, mg = require('mailgun-js')({apiKey: config.mg.api_key, domain: config.mg.domain})
	, fs = require('fs')
	, pug = require('pug')

const app = express()

app
// Setup view engine
	.set('views', `${__dirname}/views`)
	.set('view engine', 'pug')

// Setup middleware
	.use(stylus.middleware({
		src: `${__dirname}/styl`,
		dest: `${__dirname}/public/css`,
		compress: true
	}))
	.use(express.static(`${__dirname}/public`))
	.use(bodyParser.urlencoded({ extended: false }))
	.use(bodyParser.json())

// Send Routes
	.get('/', (req, res) => { res.render('index', config.routes.index) })
	.get('/coinme', (req, res) => { res.render('casestudies/coinme', config.routes.coinme) })
	.get('/trip', (req, res) => { res.render('casestudies/trip', config.routes.trip) })
	.get('/butter', (req, res) => { res.render('casestudies/butter', config.routes.butter) })
	.get('/charted', (req, res) => { res.render('casestudies/charted', config.routes.charted) })
	.get('/contact', (req, res) => { res.render('contact', config.routes.contact) })
	.get('/about', (req, res) => { res.render('about', config.routes.about) })

// Send Files
	.get('/resume', (req, res) => {
		fs.readFile('./public/resumes/resume.pdf', (err,data) => {
			res.contentType("application/pdf")
			res.send(data)
		})
	})
	.get('/resume-text', (req, res) => {
		fs.readFile('./public/resumes/Resume.txt', (err,data) => {
			res.contentType("text/plain")
			res.send(data)
		})
	})
	.get('/coinme-pdf', (req, res) => {
		fs.readFile('./public/resumes/Coinme.pdf', (err,data) => {
			res.contentType("application/pdf")
			res.send(data)
		})
	})

// Send Mail
	.post('/contact/send', (req, res) => {
		let text = `${req.body.name},\nYour message came through and you should expect to hear from me pretty soon. Below is what you sent me:\n${req.body.message}`
		let html = pug.renderFile('views/mail.pug', {
			name: req.body.name,
			message: req.body.message
		})
		let options = {
			from	: 'Konstantin Velitchko <me@kv.ax>',
			to		: req.body.email,
			bcc		: 'konstantinvelitchko@gmail.com',
			subject	: 'Your message came through!',
			html	: html,
			text	: text
		}
		mg.messages().send(options, (err) => {
			res.redirect('/')
		})
	})

// Other Routes
	.get('/sms', (req, res) => {
		res.redirect('sms://14255984796')
	})
	.get('*', (req, res) => {
		res.redirect('/')
	})

// Start Server
	.listen(config.server.port, () => console.log(`listening on port ${config.server.port}`))