module.exports = {
	server: {
		port: 9000,
	},
	mg: {
		api_key: "6798030e2fecd18690cf32abb47ba148-4412457b-c158f2bb",
		domain: "kv.ax"
	},
	routes: {
		index: {
			foldCSS		: 'css/index.fold.css',
			globalCSS	: 'css/index.global.css',
			title		: 'Konstantin Velitchko',
			description	: 'I am Konstantin Velitchko, UX designer.'
		},
		coinme: {
			foldCSS		: 'css/casestudy.fold.css',
			globalCSS	: 'css/casestudy.global.css',
			title		: 'Coinme',
			description	: ''
		},
		trip: {
			foldCSS		: 'css/casestudy.fold.css',
			globalCSS	: 'css/casestudy.global.css',
			title		: 'Metro Trip Planner',
			description	: ''
		},
		butter: {
			foldCSS		: 'css/casestudy.fold.css',
			globalCSS	: 'css/casestudy.global.css',
			title		: 'Butter Home',
			description	: ''
		},
		charted: {
			foldCSS		: 'css/casestudy.fold.css',
			globalCSS	: 'css/casestudy.global.css',
			title		: 'Charted',
			description	: ''
		},
		contact: {
			foldCSS		: 'css/contact.fold.css',
			globalCSS	: 'css/contact.global.css',
			title		: 'Contact - Konstantin Velitchko',
			description	: 'Contact me at me@kv.ax or 425-598-4796'
		},
		about: {
			foldCSS		: 'css/about.fold.css',
			globalCSS	: 'css/about.global.css',
			title		: 'About - Konstantin Velitchko',
			description	: 'Contact me at me@kv.ax or 425-598-4796'
		}
	}
}